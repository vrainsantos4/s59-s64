import { Fragment, useState, useEffect, useContext} from 'react';
import OrderTable from '../components/OrderTable';
import coursesData from '../data/coursesData';
import { Form, Button, Row, Col} from 'react-bootstrap';
import {Link,useNavigate} from 'react-router-dom';
import Modal from 'react-bootstrap/Modal';
import PageNotFound from './PageNotFound';
import UserContext from '../UserContext';
export default function Orders(){
	// Check to see if the mock data was captured
	// console.log(coursesData);

	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);
	const { user } = useContext(UserContext);
	const navigate = useNavigate();
	const [orders, setOrders] = useState([]);
	//we are goint to add an useEffect here so that in every time that we refresh our application it will fetch the updated content of our courses.

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
		       headers:{
		           Authorization: `Bearer ${localStorage.getItem('token')}`
		       }
		   })
		.then(response => response.json())
		.then(data => {
			setOrders(data.orders.map(orders => {
				// console.log(course);
				return(
					<OrderTable key = {orders._id} courseProp = {orders} />
					)
			}))
		})
	}, [])

	// The "map" method loops through the individual course objects in our array and returns a component for each course
	// Multiple components created through the map method must have a uniqe key that will help React JS indentify which components/elements have been changed
	// Everytime the map method loops through the data, it creates a "CourseCard" component and then passes the current element in our courseData array using the courseProp
	// const courses = coursesData.map(course => {
	// 	return (
	// 		<CourseCard key = {course.id} courseProp={course} />
	// 	)
	// })


	// The course in the CourseCard component is called a prop which is a shorthand for "property" since components are considered as objects in ReactJS
	// The curly braces ({}) are used for props to signify that we are providing information using JavaScript expressions rather than hard coded values which use double quotes ""
	// We can pass information fromn one component to another using props. This is refered to as props drilling
	return(
		user.isAdmin === false && user.id!== undefined
		?
		<Fragment>

			
			
			<table class="table table-striped table-dark">
				 
			  <thead>
			    <tr>
			     <th scope="col">Order ID</th>
			      <th scope="col">Product Name</th>
			      <th scope="col">Quantity</th>
			      <th scope="col">Amount</th>
			      
			     
			    </tr>
			  </thead>
			  <tbody>
					{orders}
			  </tbody>
			</table>	


			
		</Fragment>	
		:
          <PageNotFound/>
	)
}
// import Button from 'react-bootstrap/Button';
// import Form from 'react-bootstrap/Form';

import {Button, Form, Row, Col} from 'react-bootstrap';
//we need to import the useState from the react
import {useState, useEffect, useContext} from 'react';

import {useNavigate} from 'react-router-dom';
import UserContext from '../UserContext';
import PageNotFound from './PageNotFound';
import Swal2 from 'sweetalert2';
export default function Register(){
	//State hooks to store the values of the input fields

	const [productName, setProductName] = useState('');
	const [description, setDescription] = useState('');
	const [quantity, setQuantity] = useState('');
	const [price, setPrice] = useState('');
	

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const [isDisabled, setIsDisabled] = useState(true);

	useEffect(()=> {

		if(productName !== '' && description !== '' && quantity !== '' && price !== ''){
			setIsDisabled(false);
		}else{
			setIsDisabled(true);
		}
	}, [productName, description, price, quantity]);


	function addProduct(event) {


		event.preventDefault();

			        
		    	fetch(`${process.env.REACT_APP_API_URL}/products`, {
		    	            method: 'POST',
		    	            headers: {
		    	                'Content-Type' : 'application/json',
		    	                Authorization: `Bearer ${localStorage.getItem('token')}`

		    	            },
		    	            body: JSON.stringify({
		    	                productName: productName,
		    	                description: description,
		    	                price: price,
		    	                quantity: quantity
		    	               
		    	            })
		    	        })
		    	.then(response => response.json())
		    	.then(data=> {
		    	  
		    	    if(data === false){

		    	        
		    	       
		    	        Swal2.fire({
		    	            title: "Unable to add product!",
		    	            icon: 'error',
		    	           
		    	        })

		    	    }else{
		    	        

		    	        Swal2.fire({
		    	            title: 'Product has been added!',
		    	            icon: 'success',
		    	           
		    	        })

		    	       navigate('/dashboard');

		    	    }

		    	})

		    

		


		
		
	}



	return(
		user.isAdmin === true && user.id!== undefined
		?
		<Row>
			<Col className = "col-6 mx-auto">
				<h1 className = "text-center">Add Product</h1>
				<Form onSubmit ={event => addProduct(event)}>

					<Form.Group className="mb-3" controlId="formBasicFname">
					  <Form.Label>Product Name</Form.Label>
					  <Form.Control 
					  	type="Text" 
					  	placeholder="Enter product name" 
					  	value = {productName}
					  	onChange = {event => setProductName(event.target.value)}
					  	/>
					</Form.Group>

					<Form.Group className="mb-3" controlId="formBasicLname">
					  <Form.Label>Description</Form.Label>
					  <Form.Control 
					  	type="Text" 
					  	placeholder="Enter description" 
					  	value = {description}
					  	onChange = {event => setDescription(event.target.value)}
					  	/>
					</Form.Group>

				      <Form.Group className="mb-3" controlId="formBasicEmail">
				        <Form.Label>Price</Form.Label>
				        <Form.Control 
				        	type="text" 
				        	placeholder="Enter price" 
				        	value = {price}
				        	onChange = {event => setPrice(event.target.value)}
				        	/>
				       
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="formBasicMobile">
				        <Form.Label>Quantity</Form.Label>
				        <Form.Control 
				        	type="number" 
				        	placeholder="Enter Quantity" 
				        	value = {quantity}
				        	onChange = {event => setQuantity(event.target.value)}
				        	/>
				        	
				      </Form.Group>

				    

				      

				      <Button variant="primary" type="submit" disabled = {isDisabled}>
				       Submit
				      </Button>
				</Form>
			</Col>
		</Row>
		:
		<PageNotFound/>
		)
}
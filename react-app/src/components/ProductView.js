import {Row, Col, Button, Card, Form} from 'react-bootstrap';
//the useParams allows us to get or extract the parameter included in our pages
import { useParams } from 'react-router-dom';
import {useNavigate, Navigate} from 'react-router-dom';
import {useEffect, useState, useContext} from 'react';
import Swal2 from 'sweetalert2';
import UserContext from '../UserContext';
export default function ProductView(){

	const [productName, setName] = useState('');
	const [price, setPrice] = useState('');
	const [description, setDescription] = useState('');
	const [quantity, setQuantity] = useState('');
	const [isActive, setisActive] = useState('');
	const [productId, setproductId] = useState('');
	const [imgLink, setImgLink] = useState('');

	const [productNameUpdate, setProductNameUpdate] = useState('');
	const [descriptionUpdate, setDescriptionUpdate] = useState('');
	const [quantityUpdate, setQuantityUpdate] = useState('');
	const [priceUpdate, setPriceUpdate] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);
	const [isDisabled2, setIsDisabled2] = useState(true);
	const [quantityCheckOut, setQuantityCheckOut] = useState('');
	const [totalAmount, settotalAmount] = useState('');

	const { user } = useContext(UserContext);

	let ttl;
	

	const navigate = useNavigate();
	useEffect(()=> {

		if(quantityCheckOut !== ''){
			setIsDisabled2(false);
			ttl = price * quantityCheckOut
			settotalAmount(ttl)
		}else{
			setIsDisabled2(true);
		}

	}, [quantityCheckOut]);

	useEffect(()=> {

		if(productNameUpdate !== '' && descriptionUpdate !== '' && quantityUpdate !== '' && priceUpdate !== ''){
			setIsDisabled(false);
		}else{
			setIsDisabled(true);
		}
	}, [productNameUpdate, descriptionUpdate, quantityUpdate, priceUpdate]);

	const {id} = useParams();
	/*console.log(id);*/

	useEffect(()=> {
		fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
		.then(response => response.json())
		.then(data => {
			// console.log(data);
				setName(data.productName);
				setPrice(data.price);
				setDescription(data.description);
				setQuantity(data.quantity);
				setisActive(data.isActive);
				setproductId(data._id);
				setImgLink(data.imgLink);
				
		})



	}, [])

	function addProduct(event) {


		event.preventDefault();

			        
		    	fetch(`${process.env.REACT_APP_API_URL}/products/${id}`, {
		    	            method: 'PUT',
		    	            headers: {
		    	                'Content-Type' : 'application/json',
		    	                Authorization: `Bearer ${localStorage.getItem('token')}`

		    	            },
		    	            body: JSON.stringify({
		    	                productName: productNameUpdate,
		    	                description: descriptionUpdate,
		    	                price: priceUpdate,
		    	                quantity: quantityUpdate
		    	               
		    	            })
		    	        })
		    	.then(response => response.json())
		    	.then(data=> {
		    	  
		    	    if(data === false){

		    	        
		    	       
		    	        Swal2.fire({
		    	            title: "Unable to update the product!",
		    	            icon: 'error',
		    	           
		    	        })

		    	    }else{
		    	        

		    	        Swal2.fire({
		    	            title: 'Product has been updated!',
		    	            icon: 'success',
		    	           
		    	        })

		    	       navigate('/dashboard');

		    	    }

		    	})

		    

		


		
		
	}

		function checkOut(event) {

			
		event.preventDefault();

			      
			   
		    	fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
		    	            method: 'POST',
		    	            headers: {
		    	                'Content-Type' : 'application/json',
		    	                Authorization: `Bearer ${localStorage.getItem('token')}`

		    	            },
		    	            body: JSON.stringify({
		    	                productId: productId,
		    	                productName: productName,
		    	                description: description,
		    	                quantity: quantityCheckOut,
		    	               	totalAmount: totalAmount
		    	            })
		    	        })
		    	.then(response => response.json())
		    	.then(data=> {
		    	  
		    	    if(data === false){

		    	        
		    	       
		    	        Swal2.fire({
		    	            title: "Unable to checkout!",
		    	            icon: 'error',
		    	           
		    	        })

		    	    }else{
		    	        

		    	        Swal2.fire({
		    	            title: 'Order has been placed!',
		    	            icon: 'success',
		    	           
		    	        })

		    	       navigate('/orders');

		    	    }

		    	})

		    

		


		
		
	}



	return(
		user.isAdmin === true && user.id!== undefined
		?
		<Row>
			<Col className = "col-6 mx-auto">
				<h1 className = "text-center">Update Product</h1>
				<Form onSubmit ={event => addProduct(event)}>
			
				<Card>
				    <Card.Body>
				      	<Card.Title>{productName}</Card.Title>
				      		    <Form.Group className="mb-3">
				      			  <Form.Control 
				      			  	type="Text" 
				      			  	placeholder="Enter new product name" 
				      			  	value = {productNameUpdate}
				      			  	onChange = {event => setProductNameUpdate(event.target.value)}
				      			  	/>
				      			</Form.Group>
				        <Card.Text>
				          {description}
				          		    <Form.Group className="mb-3">
				          			  <Form.Control 
				          			  	type="Text" 
				          			  	placeholder="Enter new description" 
				          			  	value = {descriptionUpdate}
				          			  	onChange = {event => setDescriptionUpdate(event.target.value)}
				          			  	/>
				          			</Form.Group>
				        </Card.Text>
				         
				        <Card.Text>
				          Price: {price}
				           <Form.Group className="mb-3">
				          			  <Form.Control 
				          			  	type="Text" 
				          			  	placeholder="Enter new price" 
				          			  	value = {priceUpdate}
				          			  	onChange = {event => setPriceUpdate(event.target.value)}
				          			  	/>
				          			</Form.Group>
				        </Card.Text>

				        

				        <Card.Text>
				          Quantity: {quantity}
				           <Form.Group className="mb-3">
				          			  <Form.Control 
				          			  	type="number" 
				          			  	placeholder="Enter new quantity" 
				          			  	value = {quantityUpdate}
				          			  	onChange = {event => setQuantityUpdate(event.target.value)}
				          			  	/>
				          			</Form.Group>
				        </Card.Text>
				       
				         
				        
				      </Card.Body>
				      <Button variant="primary" type="submit" disabled = {isDisabled}>
				       Submit
				      </Button>
				 </Card>
				</Form>
			</Col>
		</Row>
		:
				<Row>
			<Col className = "col-6 mx-auto">
				<h1 className = "text-center">Checkout Product</h1>
				<Form onSubmit ={event => checkOut(event)}>
			
				<Card>
				    <Card.Body>
				    <img class="card-img-top" src={imgLink} alt="Card image cap"/>
				      	<Card.Title>{productName}</Card.Title>
				      		   
				        <Card.Text>{description}</Card.Text>
				         
				        <Card.Text>₱{price}</Card.Text>

				        

				        <Card.Text>
				          Quantity:
				           <Form.Group className="mb-3">
				          			  <Form.Control 
				          			  	type="number" 
				          			  	placeholder="Enter new quantity" 
				          			  	value = {quantityCheckOut}
				          			  	onChange = {event => setQuantityCheckOut(event.target.value)}
				          			  	/>
				          			</Form.Group>
				        </Card.Text>
				       
				        <Card.Text>Total Amount: ₱{totalAmount}</Card.Text>
				        
				      </Card.Body>
				      <Button variant="primary" type="submit" disabled = {isDisabled2}>
				       Checkout
				      </Button>
				 </Card>
				</Form>
			</Col>
		</Row>


		)
}
import { Button, Row, Col } from 'react-bootstrap';

import {Link} from 'react-router-dom';

export default function Banner(){
	return (
		<Row>
	    	<Col className="p-5 ">
	            <h1>Online Shopping</h1>
	            <p>Shop now. Pay Later</p>
	            <Button as = {Link} to = '/courses' variant="primary">See more.</Button>
	        </Col>
	    </Row>

	)
}
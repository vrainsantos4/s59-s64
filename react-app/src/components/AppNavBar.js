import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

import {useState, useEffect, useContext} from 'react';
import {NavLink, Link} from 'react-router-dom';

import UserContext from '../UserContext';

// The as keyword allows components to be treated as if they are different component gaining access to it's properties and functionalities
//The to keyword is used in place of the 'href' for providing the URL for the page
export default function AppNavBar() {

	const { user } = useContext(UserContext);

	// console.log(user);
	//In here, I will capture the value from our localStorage and then store it  a state.
	//Syntax:
		// localStorage.getItem('key/property');

	// console.log(localStorage.getItem('email'));
	// const [user, setUser] = useState(localStorage.getItem('email'));

	/*const localStorage1 = localStorage.getItem('email');*/

	/*useEffect(() => {

		setUser(localStorage.getItem('email'));

		console.log("hi");

	}, [localStorage1])*/

	let userType;
	let link;
	let userType2;
	let link2;

	if(user.isAdmin===true){

		userType = "Products"
		link = "/dashboard"

		userType2 = ""
	}
	else{
		userType = "My Orders"
		userType2 = "Home"
		link = "/orders"
		link2 = "/"
	}

	return (
		<Navbar bg="light" expand="lg">
		    <Container fluid>
		        <Navbar.Brand as = {Link} to = '/login'>IGOTSHOP PH</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav"/>
		        <Navbar.Collapse id="basic-navbar-nav">
		            <Nav className="ms-auto">
		           
		            {
		            	user.id === null || user.id ===undefined
		            	?
		            	<>
		            		<Nav.Link as = {NavLink} to = '/register'>Register</Nav.Link>
		            		<Nav.Link as = {NavLink} to = '/login'>Login</Nav.Link>
		            	</>
		            	:
		            	<>
		            	<Nav.Link as = {NavLink} to = {link2}>{userType2}</Nav.Link>
		            	<Nav.Link as = {NavLink} to = {link}>{userType}</Nav.Link>
		            	<Nav.Link as = {NavLink} to = '/logout'>Logout</Nav.Link>
		            	</>
		            }


		            </Nav>
		        </Navbar.Collapse>
		    </Container>
		</Navbar>

	)
}
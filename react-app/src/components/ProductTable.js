import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import Swal2 from 'sweetalert2';
import {Link} from 'react-router-dom';

export default function Products({courseProp}) {
	// Checks to see if the data was successfully passed
    // console.log(courseProp);

    // console.log(courseProp.name);
    /*console.log('this is from the CourseCard.js')
    console.log(courseProp);*/

    const {_id, productName, description, price,isActive,quantity} = courseProp;
    
    let avail;
    let active;
    if(isActive===true){
        avail = "Available"
        active = "Disable"
    }
    else{
        avail = "Unavailable"
        active = "Enable"
    }

    // Use the state hook for this component to be able to store its state
    // States are used to keep track of information related to individual components
    const [count, setCount] = useState(0);

    const [seats, setSeats] = useState(30);


    //we are goinbg to create a new state that will declare or tell the value of the disabled property in the button.
    const [isDisabled, setIsDisabled] = useState(false);

    // Function that keeps track of the enrollees
    // The setter function for UseState are asynchronous allowing it to execute separately from other codes in the program
    // The "setCount" function is being executed while the "console.log" is already completed
    function enroll(){
        if (seats > 1) {
            setCount(count + 1);
            // console.log('Enrollees: ' + count);
            setSeats(seats - 1);
            // console.log('Seats: ' + seats);
        } else {
            alert("Congratulations on getting the last slot!");
            setSeats(seats-1);
        };
    }

    function archiveProduct(e){

           

            fetch(`${process.env.REACT_APP_API_URL}/products/${e}`, {
                            method: 'GET',
                            headers: {
                                'Content-Type' : 'application/json',
                                Authorization: `Bearer ${localStorage.getItem('token')}`

                            }
                           
            })
             .then(response => response.json())
                .then(data=> {
                  
                  if(data.isActive === true){
                    
                    fetch(`${process.env.REACT_APP_API_URL}/products/${e}/archive`, {
                                    method: 'PATCH',
                                    headers: {
                                        'Content-Type' : 'application/json',
                                        Authorization: `Bearer ${localStorage.getItem('token')}`

                                    },
                                    body: JSON.stringify({
                                        isActive:false
                                       
                                    })
                                })
                        .then(response => response.json())
                        .then(data=> {
                            // console.log(data);
                            // localStorage.getItem('token', data);

                            //if statement to check whether the login is successful.
                            if(data === false){

                                
                                /*alert('Login unsuccessful!');*/
                                //in adding sweetlaert2 you have to use the fire method
                                Swal2.fire({
                                    title: "Unable to deactivate the product!",
                                    icon: 'error',
                                   
                                }).then(function (){
                                    refreshPage();
                                })



                            }else{
                                

                                Swal2.fire({
                                    title: 'Product has been deactivated.',
                                    icon: 'success',
                                    
                                }).then(function (){
                                    refreshPage();
                                })


                            }

                        })
                    
                  }
                  else{

                    fetch(`${process.env.REACT_APP_API_URL}/products/${e}/unarchive`, {
                                    method: 'PATCH',
                                    headers: {
                                        'Content-Type' : 'application/json',
                                        Authorization: `Bearer ${localStorage.getItem('token')}`

                                    },
                                    body: JSON.stringify({
                                        isActive:true
                                       
                                    })
                                })
                        .then(response => response.json())
                        .then(data=> {
                            // console.log(data);
                            // localStorage.getItem('token', data);

                            //if statement to check whether the login is successful.
                            if(data === false){

                                
                                /*alert('Login unsuccessful!');*/
                                //in adding sweetlaert2 you have to use the fire method
                                Swal2.fire({
                                    title: "Unable to reactivate the product!",
                                    icon: 'error',
                                   
                                }).then(function (){
                                    refreshPage();
                                })



                            }else{
                                

                                Swal2.fire({
                                    title: 'Product has been re-activated.',
                                    icon: 'success',
                                    
                                }).then(function (){
                                    refreshPage();
                                })


                            }

                        })
                  
                  }

               

            })

           

    }

    function refreshPage() {
    window.location.reload(false);
  }

    //Define a 'useEffect' hook to have 'CourseCard' component do or perform a certain task after every changes in the seats state
    //the side effect will run automatically in initial rendering and in every changes of the seats state.
    //the array in the useEffect is called the dependency array
    useEffect(()=> {

        if(seats === 0){
            setIsDisabled(true);
        }

    }, [seats]);


    return (
	  // <Card>
   //      <Card.Body>
   //          <Card.Title>{name}</Card.Title>
   //          <Card.Subtitle>Description:</Card.Subtitle>
   //          <Card.Text>{description}</Card.Text>
   //          <Card.Subtitle>Price:</Card.Subtitle>
   //          <Card.Text>{price}</Card.Text>
   //          <Card.Text>Seats : {seats}</Card.Text>
   //          <Button as= {Link} to = {`/courses/${_id}`} variant="primary" disabled={isDisabled}>see more details</Button>
   //      </Card.Body>

   //     </Card>

        <tr>
                <th scope="row">{productName}</th>
                <td>{description}</td>
                <td>{price}</td>
                <td>{quantity}</td>
                <td>{avail}</td>
                <td>
                <Button as= {Link} to = {`/products/${_id}`} variant="primary" disabled={isDisabled}>Update</Button>{" "}  
                <Button onClick={e => archiveProduct(e.target.value)} value ={_id} variant="secondary">{active}</Button>
                </td>
        </tr>

      
	)
}

// Check if the CourseCard component is getting the correct prop types
// Proptypes are used for validating information passed to a component and is a tool normally used to help developers ensure the correct information is being passed from one component to the next
// CourseCard.propTypes = {
//     // The "shape" method is sued to check if a prop object conforms to a specific shape
//     courseProp: PropTypes.shape({
//         name: PropTypes.string.isRequired,
//         description: PropTypes.string.isRequired,
//         price: PropTypes.number.isRequired
//     })
// }
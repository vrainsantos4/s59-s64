import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import Swal2 from 'sweetalert2';
import {Link} from 'react-router-dom';

export default function OrderTable({courseProp}) {
	// Checks to see if the data was successfully passed
    // console.log(courseProp);

    // console.log(courseProp.name);
    /*console.log('this is from the CourseCard.js')
    console.log(courseProp);*/

    const {_id, productName,totalAmount,quantity} = courseProp;
    
  


    return (
	  // <Card>
   //      <Card.Body>
   //          <Card.Title>{name}</Card.Title>
   //          <Card.Subtitle>Description:</Card.Subtitle>
   //          <Card.Text>{description}</Card.Text>
   //          <Card.Subtitle>Price:</Card.Subtitle>
   //          <Card.Text>{price}</Card.Text>
   //          <Card.Text>Seats : {seats}</Card.Text>
   //          <Button as= {Link} to = {`/courses/${_id}`} variant="primary" disabled={isDisabled}>see more details</Button>
   //      </Card.Body>

   //     </Card>

        <tr>
                <th scope="row">{_id}</th>
                <td>{productName}</td>
    			<td>{quantity}</td>
                <td>{totalAmount}</td>
                
               
               
        </tr>

      
	)
}

// Check if the CourseCard component is getting the correct prop types
// Proptypes are used for validating information passed to a component and is a tool normally used to help developers ensure the correct information is being passed from one component to the next
// CourseCard.propTypes = {
//     // The "shape" method is sued to check if a prop object conforms to a specific shape
//     courseProp: PropTypes.shape({
//         name: PropTypes.string.isRequired,
//         description: PropTypes.string.isRequired,
//         price: PropTypes.number.isRequired
//     })
// }